# README

## Task


Write small and quick test that enters ’https://console.argyle.com/’ and fill email field with ‘test@test.com’ and password with ‘Password123!@#’ and check for ‘Login Error message’


## Index
- [Local Setup](#setup)
    * [Package Manager](#package-manager)
    * [Node.js](#node.js)
    * [Cypress](#install-cypress)
- [Local Run](#local-run)
- [Docker](#docker)
           
## Setup
### Package Manager
* [Homebrew](https://brew.sh/index_pt-br): Command to install Homebrew
```shell script
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
```

### Node.js
Comamand to install:
```shell script
brew install node
```
Command to verify installation:
```shell script
node --version
npm --version
```

### Install Cypress
Command to install

```shell script
npm install -g cypress
```

## Local run
### Chrome
```shell script
npm run test:chrome
```
### Firefox 
```shell script
npm run test:firefox
```
### Headless
```shell script
npm run test
``` 

## Docker
### Docker Image
```shell script
docker pull cypress/included:8.3.1  
```
### Chrome
```shell script
docker run -it -v $PWD:/cypress -w /cypress  cypress/included:8.3.1 --browser chrome
```
### Firefox
```shell script
docker run -it -v $PWD:/cypress -w /cypress  cypress/included:8.3.1 --browser firefox
```
### Headless
```shell script
docker run -it -v $PWD:/cypress -w /cypress  cypress/included:8.3.1 
```
### Docker compose
```shell script
docker-compose up
``` 