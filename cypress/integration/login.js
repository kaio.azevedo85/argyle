describe('Login Scenarios for the Argyle Console Website', () => {
  beforeEach(() => {
    cy.visit('https://console.argyle.com/')
  })

  it('Validate error message when sending wrong email and password.', () => {
    const email = 'test@test.com'
    const password = 'Password123!@#'
    const message = 'Invalid email and password combination'

    cy.get('[data-hook=sign-in-email-input]').type(email)
    cy.get('[data-hook=sign-in-password-input]').type(password)
    cy.get('[data-hook=sign-in-submit-button]').click()

    cy.get('[data-hook=sign-in-email-input-helper-text]')
      .should('have.text', message)
    cy.screenshot(Cypress.currentTest.title + " - " + Cypress.browser.name)
  
  })

  it('Validate error message when sending wrong email', () => {
    const email = 'test@test.com'
    const password = 'E*E5i%wg6Lyh'
    const message = 'Invalid email and password combination'

    cy.get('[data-hook=sign-in-email-input]').type(email)
    cy.get('[data-hook=sign-in-password-input]').type(password)
    cy.get('[data-hook=sign-in-submit-button]').click()

    cy.get('[data-hook=sign-in-email-input-helper-text]')
      .should('have.text', message)
    cy.screenshot(Cypress.currentTest.title + " - " + Cypress.browser.name)
  
  })

  it('Validate error message when sending wrong password.', () => {
    const email = 'argyle-test-qa-candidate@mailinator.com'
    const password = 'Password123!@#'
    const message = 'Invalid email and password combination'

    cy.get('[data-hook=sign-in-email-input]').type(email)
    cy.get('[data-hook=sign-in-password-input]').type(password)
    cy.get('[data-hook=sign-in-submit-button]').click()

    cy.get('[data-hook=sign-in-email-input-helper-text]')
      .should('have.text', message)
    cy.screenshot(Cypress.currentTest.title + " - " + Cypress.browser.name)
  
  })  

it('Check correct Sign in .', () => {
    const email = 'argyle-test-qa-candidate@mailinator.com'
    const password = 'E*E5i%wg6Lyh'
    const title = 'Users'

    cy.get('[data-hook=sign-in-email-input]').type(email)
    cy.get('[data-hook=sign-in-password-input]').type(password)
    cy.get('[data-hook=sign-in-submit-button]').click()

    cy.get('[data-hook="users-page-title"]')
      .should('have.text', title)
    cy.screenshot(Cypress.currentTest.title + " - " + Cypress.browser.name)
  
  })      
})
